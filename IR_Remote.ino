/*
 Name:		IR_Remote.ino
 Created:	3/28/2020 12:59:01 PM
 Author:	Mohammad
*/
#include "Global.h"
#include "Function.h"

void setup() 
{
    Serial.begin(115200) ;
    pinMode(IR_Pin,OUTPUT);
	IR.begin();
    EEPROM.begin(250);
    // EEPROM
    Registerd = F.EEPROM_readString(0).toInt();
    Serial.print("Registerd: ");
    Serial.println(Registerd);
    if (Registerd == 65535)
    {
        Serial.println("Active Mode");
        ST_SSID = F.EEPROM_readString(5);
        ST_PASS = F.EEPROM_readString(100);
        IR_Token = F.EEPROM_readString(200);
        Serial.print("ST_SSID: ");
        Serial.println(ST_SSID);
        Serial.print("ST_PASS: ");
        Serial.println(ST_PASS);
        Serial.print("IR_Token: ");
        Serial.println(IR_Token);
        Connection_Type = 1;                        // IR Connect To Wifi
    }
    else
    {
        Serial.println("Config Mode");
        Connection_Type = 3;
    }
    F.PingSetup();
    Server_IP.toCharArray(server_ip,30);
}

void loop() 
{
    if (Connection_Type)
    {
        switch (Connection_Type)
        {
            case 1:
            {
                F.Connect_WiFi();
                break;
            }
            case 2:
            {
                F.Connect_Server();
                break;
            }
            case 3:
            {
                F.Start_AP();
                break;
            }
        }
    }
    if (Active)
    {
        switch (Active)
        {
            case 1:
            {
                if (client.available())
                {
                    String input = client.readStringUntil('\n');
                    Serial.print("Receive Server : ");
                    Serial.println(input);
                    if (F.Recive_Jason(input))
                    {
                        F.Command_Server_Switch();
                    }
                }
                if (millis() - Last_Check_Connection >= 10000)              // Check Every 10 Seconds
                {
                    if (WiFi.status() != WL_CONNECTED)                      // Check Wifi Connection
                    {
                        Serial.println("Wifi disconnected");
                        Wifi_Connected = 0;
                        Server_Connected = 0;
                        WiFi.disconnect(true);
                        delay(10);
                        Active = 0;
                        Connection_Type = 1;                                // Reconnect Wifi
                    }
                    else if (!client.connected() | !client)                 // Check Server Connection
                    {
                        Serial.println("Server disconnected");
                        Server_Connected = 0;
                        client.stop();
                        delay(10);
                        Active = 0;
                        Connection_Type = 2;                                // Reconnect Server
                    }
                    if (Send_ping)
                    {
                        Serial.println("Send Ping");
                        pinger.Ping(server_ip, 5);
                        Send_ping = 0;
                    }
                    Last_Check_Connection = millis();
                }
                break;
            }
            case 2:
            {
                if (!client_Ap.connected() | !client_Ap)                          // If There Isn't Any Client  
                {
                    client_Ap = wifiServer.available();                        // Listen For New Client
                }
                if (client_Ap.available())                                     // If There Is A Message From Client
                {
                    String Received_Data = client_Ap.readStringUntil('\r');    // Read Message
                    Serial.print("Receive Phone : ");
                    Serial.println(Received_Data);
                    if (F.Recive_Jason(Received_Data))                   // If Protocol Correct
                    {
                        F.Command_Phone_Switch();                        // Do The Command And Response
                    }
                }
                break;
            }
        }
    }










    if (client.available())
    {
		String input = client.readStringUntil('\n');
		Serial.print("Receive Server : ");
		Serial.println(input);
		if (F.Recive_Jason(input))
		{
			F.Hex_Code_Spliter(Pronto_Hex_Code);
			Serial.println(Hex_Code_Num);
			IR.sendPronto(Hex_Code, Hex_Code_Num);
			Hex_Code_Num = 0;
		}
    }
}

