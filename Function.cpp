#include "Global.h"
#include "Function.h"

String Function::Send_Jason(String Type_From, String Token_From, int Rule_From, String Type_To, String Token_To, int Rule_To, String Time, String Type_Info, String Action, int Command, String Status)//, String Key = "", String Info_Key = "")
{
	//link example jason 6 object 
	//https://arduinojson.org/v6/api/jsonobject/createnestedobject/

	//object jason for send
	StaticJsonDocument<500> doc;
	JsonObject Message = doc.to<JsonObject>();
	JsonObject jason_From = Message.createNestedObject("From");
	JsonObject jason_To = Message.createNestedObject("To");
	JsonObject jason_Info = Message.createNestedObject("Info");
	JsonObject jason_Data = jason_Info.createNestedObject("Data");

	jason_From["Type"] = Type_From;
	jason_From["Token"] = Token_From;
	jason_From["Rule"] = Rule_From;
	jason_To["Type"] = Type_To;
	jason_To["Token"] = Token_To;
	jason_To["Rule"] = Rule_To;
	Message["Time"] = Time;
	jason_Info["Type"] = Type_Info;
	jason_Info["Action"] = Action;
	jason_Info["Command"] = Command;
	switch (Command)
	{
		case 5:
		{
			jason_Data["Test"] = WiFi.RSSI();
			break;
		}
		case 22:
		{
			jason_Data["Type"] = Device_Type;
			jason_Data["Model"] = Device_Model;
			jason_Data["Series"] = Hex_Code_Series;                                             // Get Mac Address
			jason_Data["Key"] = Key;
			jason_Data["Pronto"] = Pronto_Hex_Code;
		}
	}
	String finalJson = "";
	serializeJson(Message, finalJson);
	return (finalJson + "\n");
}

bool Function::Recive_Jason(String massage)
{
	//link example jason 6 object 
	//https://arduinojson.org/v6/api/jsonobject/createnestedobject/
	//define data to object jason1
	DynamicJsonDocument Massage(2048);
	deserializeJson(Massage, massage);
	String From = Massage["From"];
	String To = Massage["To"];
	String Info = Massage["Info"];
	DynamicJsonDocument jason_From(100);
	char from[50];
	From.toCharArray(from, 50);
	char to[50];
	To.toCharArray(to, 50);
	char info[350];
	Info.toCharArray(info, 350);
	deserializeJson(jason_From, from);
	DynamicJsonDocument jason_To(100);
	deserializeJson(jason_To, to);
	DynamicJsonDocument jason_Info(1024);
	deserializeJson(jason_Info, info);
	String Data = jason_Info["Data"];
	char data[300];
	Data.toCharArray(data, 300);
	DynamicJsonDocument jason_Data(512);
	deserializeJson(jason_Data, data);

	// Values 
	String type_from = jason_From["Type"];
	String Token_from = jason_From["Token"];
	if (Token_from == "null")
	{
		Serial.println("Protocol_Error1");
		return 0;
	}
	Token_From = Token_from;
	int rule_from = jason_From["Rule"];
	String type_to = jason_To["Type"];
	String Token_to = jason_To["Token"];
	int rule_to = jason_To["Rule"];
	String time = Massage["Time"];
	String type_info = jason_Info["Type"];
	String action = jason_Info["Action"];
	int command = jason_Info["Command"];
	if (type_from == "null" | type_to == "null" | Token_to == "null" | time == "null" | type_info == "null" | action == "null" | command == NULL)   // Check Protocol
	{
		Serial.println("Protocol_Error2");
		return 0;
	}
	Type_From = type_from;
	Rule_From = rule_from;
	Type_To = type_to;
	Token_To = Token_to;
	Rule_To = rule_to;
	Time = time;
	Type_Info = type_info;
	Action = action;
	Command = command;
	switch (command)																							// Detect Command Type
	{
		case 22:
		{
			String type = jason_Data["Type"];
			String model = jason_Data["Model"];
			String series = jason_Data["Series"];
			String key = jason_Data["Key"];
			String pronto = jason_Data["Pronto"];
			if (type == "null" | model == "null" | series == "null" | key == "null" | pronto == "null")
			{
				Serial.println("Protocol_Error3");
				return 0;
			}
			Device_Type = type;
			Device_Model = model;
			Hex_Code_Series = series;
			Key = key;
			Pronto_Hex_Code = pronto;
			Serial.println(Pronto_Hex_Code);
			break;
		}
	}
	return 1;
}
void Function::Hex_Code_Spliter(String pronto)
{
	int Start = 1;
	for (int i = 0; i < pronto.length(); i++)
	{
		if (pronto[i] == ',')
		{
			Hex_Code[Hex_Code_Num] = pronto.substring(Start, i).toInt();
			Serial.print(Hex_Code[Hex_Code_Num]);
			Serial.print(",");
			Hex_Code_Num++;
			Start = i + 1;
		}
	}
}

void Function::EEPROM_writeString(char add, String data)
{
	int _size = data.length();
	int i;
	for (i = 0; i < _size; i++)
	{
		EEPROM.write(add + i, data[i]);
	}
	EEPROM.write(add + _size, '\0');   //Add termination null character for String Data
	EEPROM.commit();
}


String Function::EEPROM_readString(char add)
{
	int i;
	char data[500]; //Max 100 Bytes
	int len = 0;
	unsigned char k;
	k = EEPROM.read(add);
	while (k != '\0' && len < 500)   //Read until null character
	{
		k = EEPROM.read(add + len);
		data[len] = k;
		len++;
	}
	data[len] = '\0';
	return String(data);
}

void Function::Connect_WiFi()
{
	if (try_wifi++ >= 100)
	{
		char SSID[100];
		char PASS[100];
		ST_SSID.toCharArray(SSID, 100);
		ST_PASS.toCharArray(PASS, 100);
		WiFi.mode(WIFI_STA);
		delay(10);
		WiFi.begin(SSID, PASS);
		Serial.print("Waiting for WiFi... ");
		Serial.print("Connecting to ");
		Serial.println(ST_SSID);
		Wifi_Connected = 0;
		try_wifi = 0;
	}
	delay(100);
	if (WiFi.status() != WL_CONNECTED)
	{
		Connection_Type = 1;
	}
	else
	{
		Serial.println("WiFi connected");
		Serial.print("IP address: ");
		Serial.println(WiFi.localIP());
		Wifi_Connected = 1;
		Connection_Type = 2;								// Connect To Server 
		try_wifi = 100;
	}
}

void Function::Connect_Server()
{
	Serial.print("Connecting to : ");
	Serial.println(Server_IP);
	char IP[1000];
	Server_IP.toCharArray(IP, 1000);
	client.setTimeout(100);
	client.connect(IP, Server_Port.toInt());
	delay(20);
	if (!client.connected())
	{
		Server_Connected = 0;
		Serial.println("Not Connect To Server");
		Connection_Type = 2;							// Reconnect To Server
		Active = 0;
	}
	else
	{
		delay(10);
		Server_Connected = 1;
		Serial.println("Connected to Server");
		Connection_Type = 0;
		Active = 1;								// Active In Client Mode
		String Log_In = Send_Jason(IR_Type, IR_Token, 1, "S", Server_ID, 1, "2019-12-01 12:22:21", "Request", "Get", 5, String(""));
		Send_To_Server(Log_In);
		Send_ping = 1;
	}
}

void Function::Start_AP()
{
	Serial.println("Start Access Point");
	if (client.connected() | client)
	{
		client.stop();
	}
	if (WiFi.status() == WL_CONNECTED)
	{
		WiFi.disconnect(true);
	}
	char SSID[100];
	char PASS[100];
	AP_SSID.toCharArray(SSID, 100);
	AP_PASS.toCharArray(PASS, 100);
	WiFi.mode(WIFI_AP);
	delay(50);
	WiFi.softAP(SSID, PASS);					// Start Access Point
	IPAddress myIP = WiFi.softAPIP();
	Serial.println(myIP);
	delay(10);
	wifiServer = WiFiServer(IPAddress(192, 168, 4, 1), 5665);
	delay(10);
	wifiServer.begin();									// Start Server
	delay(10);
	Config_Mode = 1;
	Server_Connected = 0;
	Wifi_Connected = 0;
	Connection_Type = 0;
	Active = 2;									// Active In Access Point Mode
}

void Function::Send_To_Server(String Massage)
{
	Serial.print("Send To Server : ");
	Serial.println(Massage);
	char massage[700];
	Massage.toCharArray(massage, 700);
	client.print(massage);
}

void Function::Command_Phone_Switch()
{
	if (Type_To == IR_Type)
	{
		ST_SSID = Given_ssid;
		ST_PASS = Given_pass;
		User = Given_User;
		Rule = Given_Rule;
		IR_Token = Given_Token;
		String Res = Send_Jason(IR_Type, IR_Token, 1, Type_From, Token_From, Rule_From, Time, "Response", Action, Command, "");
		Serial.println(Res);
		char res[1000];
		Res.toCharArray(res, 1000);
		client.print(res);
		delay(100);
		Registerd = 65535;								// Save In EEPROM
		EEPROM_writeString(0, String(Registerd));
		EEPROM_writeString(5, ST_SSID);
		EEPROM_writeString(100, ST_PASS);
		EEPROM_writeString(200, IR_Token);
		Serial.print("SSID : ");
		Serial.println(ST_SSID);
		Serial.print("PASS : ");
		Serial.println(ST_PASS);
		Config_Mode = 0;
		WiFi.softAPdisconnect(true);				// Turn Off Access Point
		delay(10);
		Connection_Type = 1;						// Connect To Wifi 
		Active = 0;
	}
}

void Function::Command_Server_Switch()
{
	if (Type_To == IR_Type && Token_To == IR_Token)
	{
		switch (Command)
		{
			case 22:
			{
				if (Type_Info == "Request")
				{
					if (Action == "Set")
					{
						Serial.println("22-Request-Set");
						Hex_Code_Spliter(Pronto_Hex_Code);
						IR.sendPronto(Hex_Code, Hex_Code_Num);
						Serial.println("Send IR Code");
						Hex_Code_Num = 0;
						String Res = Send_Jason(IR_Type, IR_Token, 1, Type_From, Token_From, Rule_From, Time, "Response", Action, Command, String(""));
						Send_To_Server(Res);
					}
				}
				break;
			}
		}
	}
}

void Function::PingSetup()
{
	//#include <lwip/icmp.h> // needed for icmp packet definitions
	pinger.OnReceive([](const PingerResponse& response)
		{
			return true;
		});
	pinger.OnEnd([](const PingerResponse& response)
		{
			// Evaluate lost packet percentage
			float loss = 100;
			if (response.TotalReceivedResponses > 0)
			{
				loss = (response.TotalSentRequests - response.TotalReceivedResponses) * 100 / response.TotalSentRequests;
			}
			if (loss == 100)
			{
				// Internet Not available
				Serial.println("Internet Not Available");
				if (!Connection_Type)
				{
					Server_Connected = 0;
					client.stop();
					Active = 0;
					Connection_Type = 2;                                 // Reconnect Server
				}
			}
			if (Server_Connected)
			{
				Send_ping = 1;
			}
			return true;
		});
}