#pragma once
#include <PingerResponse.h>
#include <Pinger.h>
#include <ArduinoJson.hpp>
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <IRsend.h>
#include <IRremoteESP8266.h>
#include <EEPROM.h>
#include "Function.h"

extern int IR_Pin;
extern uint16_t Hex_Code[100];
extern String Pronto_Hex_Code;
extern String Device_Type;
extern String Device_Model;
extern String Hex_Code_Series;
extern String Key;
extern int Hex_Code_Num;
extern int Registerd;
extern int Connection_Type;
extern int try_wifi;
extern unsigned long Last_Check_Connection;

extern bool Wifi_Connected;
extern bool Server_Connected;
extern bool Config_Mode;
extern bool Send_ping;
extern int Active;

extern String ST_SSID;
extern String ST_PASS;
extern String AP_SSID;
extern String AP_PASS;
extern String IR_Token;
extern String Given_ssid;
extern String Given_pass;
extern String Given_User;
extern int Given_Rule;
extern String Given_Token;
extern String User;
extern int Rule;
extern String IR_Type;

extern String Server_IP;
extern char server_ip[30];
extern String Server_Port;
extern String Server_ID;

extern String Type_From;
extern String Token_From;
extern int Rule_From;
extern String Type_To;
extern String Token_To;
extern int Rule_To;
extern String Time;
extern String Type_Info;
extern String Action;
extern int Command;

extern IRsend IR;
extern WiFiClient client;
extern WiFiClient client_Ap;
extern WiFiServer wifiServer;
extern Function F;
extern Pinger pinger;

class Global
{
};

